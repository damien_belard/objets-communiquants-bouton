int pin = 13;
volatile int state = HIGH;


void setup() {
  // put your setup code here, to run once:
  pinMode(pin, OUTPUT);
  attachInterrupt(0, blink, CHANGE);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(pin, state);
}

void blink(){
  state = !state;
}
